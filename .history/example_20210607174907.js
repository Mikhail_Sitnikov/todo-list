let AddTaskBtn = $("#add");
let DeskTaskInput = $("#message");
let todo = $("#todo");
let get__id= $("#get__get");
let curPage = 1;
let curPageForCompleted = 1;
let activeStatus = 'all';
// let i = 0;
let globalID = function () {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return '_' + Math.random().toString(36).substr(2, 9);
};
let state = {
  tasks: [],
}
let TodoList = [];
let ActiveTodo=[];
let CompletedTodo=[];


$('.add').on('click', function(){
  if (!DeskTaskInput.val().trim()) return;
  addNewTodoItem(DeskTaskInput.val().trim());
  renderAllTodos();
});

//добавление todo по enter
$(document).on('keydown',function(e) {
  if(e.which == 13) {
    if (!DeskTaskInput.val().trim()) return;
    addNewTodoItem(DeskTaskInput.val().trim());
    renderAllTodos();
  }
});
//добавление todo по enter

function addNewTodoItem(value) {
    if (!value) return;

    let newTodo = {
      title: value,
      checked: false,
      id: globalID()
    };
    TodoList.push(newTodo);
    CompletedTodo.push(newTodo); 
    ActiveTodo.push(newTodo); 
    DeskTaskInput.val('');
};


$('.pagination').on('click', '.page-link', function(event){
  let clickNum = $(event.currentTarget).data('num');
  curPage = clickNum;
  renderAllTodos();
})



$('#btn-on').click(function(){
	TodoList.forEach((todo) => {
    todo.checked = true;
  });
  renderAllTodos();
});

$('#btn-off').on('click', function(){
  TodoList = TodoList.filter((todo) => todo.checked == false);
  renderAllTodos();
});

$('#todo').on('click','.todo-check',function(event){
  let $currEl = $(event.currentTarget);
  let id = $currEl.data('id');
  let curTodo = TodoList.find((todo) => todo.id == id);
  curTodo.checked = !curTodo.checked;
  updateCounters();
})


function updateCounters(){
  let $active = $('.counter.active span');
  let $completed = $('.counter.completed span');
  let $all = $('.counter.all span');
  $all.html(TodoList.length);
  $completed.html(TodoList.filter((todo) => todo.checked == true).length);
  $active.html(TodoList.filter((todo) => todo.checked == false).length);
}

function renderAllTodos(){
  let todosHTML = ''
  let currentTodoForPage = [];
  let todoListForActiveTab = [];

  if (activeStatus == 'all') {
    todoListForActiveTab = TodoList;
  } else if (activeStatus == 'completed'){
    todoListForActiveTab = TodoList.filter((todo) => todo.checked === true)
  } else {
    todoListForActiveTab = TodoList.filter((todo) => todo.checked === false)
  }
  
  currentTodoForPage = todoListForActiveTab.slice((curPage - 1) * 5 , curPage * 5);
  currentTodoForPage.forEach((todo) => {
    todosHTML += `<div class="div_container" id="${todo.id}"><li class='li page-item disabled ' data-id="${todo.id}">
      <input type='checkbox' class="todo-check" data-id="${todo.id}" ${todo.checked ? 'checked' : ''}>
      <label>${todo.title}</label>
      <input class='edit-title' type='text'/>   
      <span class="task__delete"> <img data-id="${todo.id}" src="icon.png"/> </span>
      </li></div>`;
  })
  updateCounters();
  
  $('#todo').html(todosHTML);

  let pageNumber = Math.ceil(todoListForActiveTab.length / 5); 
  $('.pagination').html('');
  for(let i=1; i <= pageNumber; i++){
    $('.pagination').append(` <li class="page-item"><a class="page-link" href="#" data-num="${i}">${i}</a></li>`)
  }
}



$('.btn-status-change').on('click', function(event){
  activeStatus = $(event.currentTarget).data('status');
  renderAllTodos();
});

//$('#todo').on('dblclick','.todo-check',function(event){
  //let $currEl = $(event.currentTarget);
  //let id = $currEl.data('id');
  //let ippa = TodoList.find((todo) => todo.id == id);
  //ippa.title = "123";
//  console.log(ippa);
  //renderAllTodos(); 
//});


$('#todo').on('click','img',function(event){
  let curretId = $(event.currentTarget).data('id');
  console.log(curretId);
  TodoList.splice(TodoList.findIndex((todo) => todo.id == curretId), 1);
  renderAllTodos();
});

$('#todo').on('dblclick', '.page-item', function(event){
  console.log($(this).data('id'));
  $(this).addClass('edit');
  let $currEl = $(event.currentTarget);
  let id = $currEl.data('id');
  let currentTodo= TodoList.find((todo) => todo.id == id);
  let  a = $(this).find('.edit-title').val(currentTodo.title);
  currentTodo.title = DeskTaskInput.val(); 
    
  });

  $(".edit-title").on('keydown',function(e) {
    if(e.which == 13) {
      $('.edit-title').removeClass('edit');
    }}
)

