let AddTaskBtn = $("#add");
let DeskTaskInput = $("#message");
let todo = $("#todo");
let get__id= $("#get__get");
let curPage = 1;
let activeStatus = 'all';
// let i = 0;
let globalID = function () {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return '_' + Math.random().toString(36).substr(2, 9);
};
let state = {
  tasks: [],
}
let TodoList = [];
let ActiveTodo=[];
let CompletedTodo=[];


$('.add').on('click', function(){
  if (!DeskTaskInput.val().trim()) return;
  addNewTodoItem(DeskTaskInput.val().trim());
  renderAllTodos();
});

//добавление todo по enter
$(document).on('keydown',function(e) {
  if(e.which == 13) {
    if (!DeskTaskInput.val().trim()) return;
    addNewTodoItem(DeskTaskInput.val().trim());
    renderAllTodos();
  }
});
//добавление todo по enter

function addNewTodoItem(value) {
    if (!value) return;

    let newTodo = {
      title: value,
      checked: false,
      id: globalID()
    };
    TodoList.push(newTodo);
    CompletedTodo.push(newTodo); 
    ActiveTodo.push(newTodo); 
    DeskTaskInput.val('');
};
function renderAllTodos(){
  let todosHTML = ''
  let currentTodoForPage = [];

  currentTodoForPage = TodoList.slice((curPage - 1) * 5 , curPage * 5);
  currentTodoForPage.forEach((todo) => {
    todosHTML += `<div class="div_container" id="${todo.id}"><li class='li page-item disabled '>
      <input type='checkbox' class="todo-check" data-id="${todo.id}" ${todo.checked ? 'checked' : ''}>
      <label>${todo.title}</label>   
      <span class="task__delete"> <img src="icon.png"/> </span>
      </li></div>`;
  })
  updateCounters();
  
  $('#todo').html(todosHTML);

  let pageNumber = Math.ceil(TodoList.length / 5); 
  $('.pagination').html('');
  for(let i=1; i <= pageNumber; i++){
    $('.pagination').append(` <li class="page-item"><a class="page-link" href="#" data-num="${i}">${i}</a></li>`)
  }
}

$('.pagination').on('click', '.page-link', function(event){
  let clickNum = $(event.currentTarget).data('num');
  curPage = clickNum;
  renderAllTodos();
})



$('#btn-on').click(function(){
	TodoList.forEach((todo) => {
    todo.checked = true;
  });
  renderAllTodos();
});

$('#btn-off').on('click', function(){
  TodoList = TodoList.filter((todo) => todo.checked == false);
  renderAllTodos();
});

$('#todo').on('click','.todo-check',function(event){
  let $currEl = $(event.currentTarget);
  let id = $currEl.data('id');
  let curTodo = TodoList.find((todo) => todo.id == id);
  curTodo.checked = !curTodo.checked;
  updateCounters();
})


function updateCounters(){
  let $active = $('.counter.active span');
  let $completed = $('.counter.completed span');
  let $all = $('.counter.all span');
  $all.html(TodoList.length);
  $completed.html(TodoList.filter((todo) => todo.checked == true).length);
  $active.html(TodoList.filter((todo) => todo.checked == false).length);
}
//Отображение всего списка, либо только выполненых, либо только невыполненных (All Active Completed)
function renderActiveTodos(){
  let todosHTML = ''
  let currentTodoForPage = [];

  currentTodoForPage = ActiveTodo.slice((curPage - 1) * 5 , curPage * 5);
  currentTodoForPage.forEach((todo) => {
    todosHTML += `<div class="div_container" id="${todo.id}"><li class='li page-item disabled '>
      <input type='checkbox' class="todo-check" data-id="${todo.id}" ${todo.checked ? 'checked' : ''}>
      <label>${todo.title}</label>   
      <span class="task__delete"> <img src="icon.png"/> </span>
      </li></div>`;
  })
  updateCounters();
  
  $('#todo').html(todosHTML);

  let pageNumber = Math.ceil(ActiveTodo.length / 5); 
  $('.pagination').html('');
  for(let i=1; i <= pageNumber; i++){
    $('.pagination').append(` <li class="page-item"><a class="page-link" href="#" data-num="${i}">${i}</a></li>`)
  }
}
function renderCompletedTodos(){
  let todosHTML = ''
  let currentTodoForPage = [];

  currentTodoForPage = CompletedTodo.slice((curPage - 1) * 5 , curPage * 5);
  currentTodoForPage.forEach((todo) => {
    todosHTML += `<div class="div_container" id="${todo.id}"><li class='li page-item disabled '>
      <input type='checkbox' class="todo-check" data-id="${todo.id}" ${todo.checked ? 'checked' : ''}>
      <label>${todo.title}</label>   
      <span class="task__delete"> <img src="icon.png"/> </span>
      </li></div>`;
  })
  updateCounters();
  
  $('#todo').html(todosHTML);

  let pageNumber = Math.ceil(CompletedTodo.length / 5); 
  $('.pagination').html('');
  for(let i=1; i <= pageNumber; i++){
    $('.pagination').append(` <li class="page-item"><a class="page-link" href="#" data-num="${i}">${i}</a></li>`)
  }
}

$('#ActiveTodos').on('click', function(){
  ActiveTodo = ActiveTodo.filter((todo) => todo.checked == true);
  renderActiveTodos();
});
$('#AllTodo').on('click', function(){
  renderAllTodos();

});
$('#CompletedTodos').on('click', function(){
  CompletedTodo= CompletedTodo.filter((todo) => todo.checked == false);
  renderCompletedTodos();
});

//Отображение всего списка, либо только выполненых, либо только невыполненных (All Active Completed)

// $("html").on('click',".123", function(){
//   for (j = 0; j< TodoList.length; ++j) {
//   if(TodoList[j].checked == false)
//   {
//     TodoList[j].checked = true;
//     console.log(TodoList[j].checked);}
//   else {
//     TodoList[j].checked = false;
//     console.log(TodoList[j].checked)}
// }});
// let massive= []








// $('#btn-off').on('click', function(){
//   for (j = 0; j< TodoList.length; ++j) {
//     if(TodoList[j].checked == true)
//     {
//     console.log(TodoList[j]);
//     TodoList.splice(TodoList[j],1);
//     console.log(TodoList);
//     $(TodoList[j]).closest(TodoList[j]).remove(TodoList[j]);
//   }
// }});

// $("#todo").on("click","img",function(){
//   const id = $(this).closest("div").attr("id");
//   console.log(id);
//   TodoList.splice(id,1);
//   $(this).closest("div").remove();
// })



// $("html").on("click","input",function(){
//   if ($(this).closest("div").attr("class")=="div_container"){
//     if($(this).attr("checked")=="checked"){

//     }
//     else{
//       var $id= ($(this).attr("id")).slice(-1);

      
//     }
//   }

// })


/*
$('#btn-off').click(function(){
  if($(`#item_${Date.now()}`).is(':checked')){
  $(`#item_${Date.now()}`).remove(`#item_${Date.now()}`)}
});
*/

//функция нахождения checked'a

/*
  let t = TodoList[TodoList.length - 1];
  console.log(TodoList[TodoList.length - 1]);
  j++;
  massive.push($('input.radioListMode:checked').val());
  $('TodoList[TodoList.length - 1]')
  if ($('body input:checkbox').is(':checked')) {
  mass_checked.push(t);
    };
*/
  /*$(".task__delete").on('click', function (event){
    const id = $(this).closest("li").attr("class");
   console.log(id);
   if ($(this).attr("class")==id) {}
  })
  */

/*let get = $(`#get_${j}`);
$('.list-group.pagination').on('click', get, function(event) {
  if (event.target.nodeName === "INPUT") {
    console.log(TodoList)
  }
  // const isCheck = get.checked;
//  massive.push($('input.radioListMode:checked').val());
});
*/
$('.todo').on({
  mouseenter: function () {
  var listElem = $(this);
      listElem.append('<div class="hover-edit-menu"><a href="#" class="edit-button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> <a href="#" class="remove-button"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></div>');
  
  // удаление определенного элемента списка ol из DOM и из localStorage	
  $('.hover-edit-menu').on('click', '.remove-button', function(){
    var elem = $(this).closest('.item', '.todo-list');
    localStorage.removeItem("taskId_" + elem.attr("data-item"));
    elem.remove();
  });	    
  
  // редактирование значения элемента списка
  $('.hover-edit-menu').on('click', '.edit-button', function(){
    var currentTask = listElem;
    var currentTaskValue = currentTask.text();
    var newTaskValue;

    // добавляем поле редактирования и подставляем в него текущий value
    currentTask.html('<input type="text" class="form-control edit-input" value="' + currentTaskValue + '"> <a href="#" class="save-button"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a> <a href="#" class="cancel-button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>'); 

    // сохраняем изменение в поле редактирования
    $('.save-button').on('click', function(){
      newTaskValue = $('.edit-input').val();
      localStorage.setItem('taskId_' + listElem.attr('data-item'), newTaskValue);
      currentTask.text(newTaskValue);
    });

    // отменяем введенные изменения
    $('.cancel-button').on('click', function(){
      currentTask.text(currentTaskValue);
    });				
  });	
  
  // скрываем edit-menu если поле редактирования открыто
  if($('.edit-input').is(':visible')) {
    $('.hover-edit-menu').hide();
  }
  },

  mouseleave: function () {
      $('.hover-edit-menu').remove();        
  }

}, '.item');