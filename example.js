const DeskTaskInput = $('#message');
let curPage = 1;
let activeStatus = 'all';
const globalID = function() {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return `_${Math.random().toString(36).substr(2, 9)}`;
};

let TodoList = [];
const ActiveTodo = [];
const CompletedTodo = [];

function addNewTodoItem(value) {
  if (!value) return;

  const newTodo = {
    title: value,
    checked: false,
    id: globalID(),
  };
  TodoList.push(newTodo);
  CompletedTodo.push(newTodo);
  ActiveTodo.push(newTodo);
  DeskTaskInput.val('');
}

function updateCounters() {
  const $active = $('.counter.active span');
  const $completed = $('.counter.completed span');
  const $all = $('.counter.all span');
  $all.html(TodoList.length);
  $completed.html(TodoList.filter((todo) => todo.checked === true).length);
  $active.html(TodoList.filter((todo) => todo.checked === false).length);
}
$('#todo').on('click', '.todo-check', (event) => {
  const $currEl = $(event.currentTarget);
  const id = $currEl.data('id');
  const curTodo = TodoList.find((todo) => todo.id === id);
  curTodo.checked = !curTodo.checked;
  updateCounters();
});

function renderAllTodos() {
  let todosHTML = '';
  let currentTodoForPage = [];
  let todoListForActiveTab = [];

  if (activeStatus === 'all') {
    todoListForActiveTab = TodoList;
  } else if (activeStatus === 'completed') {
    todoListForActiveTab = TodoList.filter((todo) => todo.checked === true);
  } else {
    todoListForActiveTab = TodoList.filter((todo) => todo.checked === false);
  }

  currentTodoForPage = todoListForActiveTab.slice((curPage - 1) * 5, curPage * 5);
  currentTodoForPage.forEach((todo) => {
    todosHTML += `<div class="div_container" id="${todo.id}"><li class='li page-item disabled ' data-id="${todo.id}">
      <input type='checkbox' class="todo-check" data-id="${todo.id}" ${todo.checked ? 'checked' : ''}>
      <label>${todo.title}</label>
      <input class='edit-title' type='text'/>   
      <span class="task__delete"> <img data-id="${todo.id}" src="icon.png"/> </span>
      </li></div>`;
  });
  updateCounters();
  $('#todo').html(todosHTML);
  const pageNumber = Math.ceil(todoListForActiveTab.length / 5);
  $('.pagination').html('');
  for (let i = 1; i <= pageNumber; i += 1) {
    $('.pagination').append(` <li class="page-item"><a class="page-link" href="#" data-num="${i}">${i}</a></li>`);
  }
}
$('.pagination').on('click', '.page-link', (event) => {
  const clickNum = $(event.currentTarget).data('num');
  curPage = clickNum;
  renderAllTodos();
});

$('#btn-on').click(() => {
  TodoList.forEach((todo) => {
    todo.checked = true;
  });
  renderAllTodos();
});

$('#btn-off').on('click', () => {
  TodoList = TodoList.filter((todo) => todo.checked === false);
  renderAllTodos();
});

$('.add').on('click', () => {
  if (!DeskTaskInput.val().trim()) return;
  addNewTodoItem(DeskTaskInput.val().trim());
  renderAllTodos();
});

$(document).on('keydown', (e) => {
  if (e.which === 13) {
    if (!DeskTaskInput.val().trim()) return;
    addNewTodoItem(DeskTaskInput.val().trim());
    renderAllTodos();
  }
});

$('.btn-status-change').on('click', (event) => {
  activeStatus = $(event.currentTarget).data('status');
  renderAllTodos();
});

$('#todo').on('click', 'img', (event) => {
  const curretId = $(event.currentTarget).data('id');
  console.log(curretId);
  TodoList.splice(TodoList.findIndex((todo) => todo.id === curretId), 1);
  renderAllTodos();
});

$('#todo').on('dblclick', '.page-item', function (event) {
  console.log($(this).data('id'));
  $(this).addClass('edit');
  $('.edit-title').on('keydown', function (e) {
    if (e.which === 13) {
      console.log('Вы нажали на кнопку?');
      const $currEl = $(event.currentTarget);
      const id = $currEl.data('id');
      const currentTodo = TodoList.find((todo) => todo.id === id);
      currentTodo.title = $(this).val();
      renderAllTodos();
      $('.edit-title').removeClass('.edit');
    }
  });
});
